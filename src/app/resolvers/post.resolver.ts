import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { empty } from 'rxjs';
import { IPost } from 'src/app/shared/models/posts/post';
import { PostService } from 'src/app/services/post/post.service';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class PostResolver implements Resolve<IPost> {

    constructor(
        private postService: PostService,
        private router: Router
    ) {}

    resolve(next: ActivatedRouteSnapshot) {
        return this.postService.getPost(next.params.id).pipe(
            map(res => res),
            catchError(
              err => {
                alert('Oops, something went wrong. ' + err.name);
                this.router.navigate(['']);

                return empty();
              }
            )
          );
    }
}
