import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { empty } from 'rxjs';
import { IUser } from 'src/app/shared/models/users/user';
import { UserService } from 'src/app/services/user/user.service';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class UserResolver implements Resolve<IUser> {

    constructor(
        private userService: UserService,
        private router: Router
    ) {}

    resolve(next: ActivatedRouteSnapshot) {
        return this.userService.getUser(next.params.id).pipe(
            map(res => res),
            catchError(
              err => {
                alert('Oops, something went wrong. ' + err.name);
                this.router.navigate(['']);

                return empty();
              }
            )
          );
    }
}
