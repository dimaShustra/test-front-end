import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListingComponent } from './components/listing/listing.component';
import { CommentComponent } from './components/comment/comment.component';
import { UserViewComponent } from './components/user-view/user-view.component';
import { PostViewComponent } from './components/post-view/post-view.component';
import { PostService } from './services/post/post.service';
import { UserService } from './services/user/user.service';
import { UserResolver } from './resolvers/user.resolver';
import { PostResolver } from './resolvers/post.resolver';

@NgModule({
  declarations: [
    AppComponent,
    ListingComponent,
    CommentComponent,
    UserViewComponent,
    PostViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [PostService, UserService, UserResolver, PostResolver],
  bootstrap: [AppComponent]
})
export class AppModule { }
