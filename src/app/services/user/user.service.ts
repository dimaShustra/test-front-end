import { Injectable } from '@angular/core';
import { IUser } from 'src/app/shared/models/users/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { endpoint } from 'src/environments/endpoints';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public getUser(id: string): Observable<IUser> {
    const url = environment.apiUrl + endpoint.users.get + id;

    return this.http.get<IUser>(url);
  }
}
