import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { endpoint } from 'src/environments/endpoints';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IPost } from 'src/app/shared/models/posts/post';
import { IComment } from 'src/app/shared/models/posts/comment';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  public getPostsAll(): Observable<IPost[]> {
    const url = environment.apiUrl + endpoint.posts.getAll;

    return this.http.get<IPost[]>(url);
  }

  public getPost(id: string): Observable<IPost> {
    const url = environment.apiUrl + endpoint.posts.get + id;

    return this.http.get<IPost>(url);
  }

  public getComments(id: string): Observable<IComment[]> {
    // Api doesn't work
    const url = environment.apiUrl + endpoint.posts.get + id + '/comments';

    return this.http.get<IComment[]>(url).pipe(
      map(res => {
        return res.filter(x => x.postId === +id);
      }));
  }




}
