import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingComponent } from './components/listing/listing.component';
import { UserViewComponent } from './components/user-view/user-view.component';
import { PostViewComponent } from './components/post-view/post-view.component';
import { UserResolver } from './resolvers/user.resolver';
import { PostResolver } from './resolvers/post.resolver';

const routes: Routes = [
  { path: '', component: ListingComponent },
  { path: 'user/:id', component: UserViewComponent, resolve: { user: UserResolver } },
  { path: 'post/:id', component: PostViewComponent, resolve: { post: PostResolver } },
  { path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
