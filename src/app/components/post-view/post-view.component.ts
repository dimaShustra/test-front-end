import { Component, OnInit } from '@angular/core';
import { IPost } from 'src/app/shared/models/posts/post';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { IUser } from 'src/app/shared/models/users/user';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss']
})
export class PostViewComponent implements OnInit {
  public post: IPost;
  public user: IUser;
  constructor(private route: ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    this.post = this.route.snapshot.data.post;
    this.userService.getUser(this.post.userId.toString()).subscribe(res => this.user = res);
  }

}
