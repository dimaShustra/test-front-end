import { Component, OnInit } from '@angular/core';
import { IUser } from 'src/app/shared/models/users/user';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent implements OnInit {
  public user: IUser;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.user = this.route.snapshot.data.user;
  }

  public get userAddress(): string {
    return `${this.user.address.city}, ${this.user.address.street}, ${this.user.address.suite}, ${this.user.address.zipcode}`;
  }

}
