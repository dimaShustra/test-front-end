import { Component, OnInit, Input } from '@angular/core';
import { IComment } from 'src/app/shared/models/posts/comment';
import { PostService } from 'src/app/services/post/post.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {
  @Input() postId: string;
  public comments: IComment[];

  private timeNow: number;
  constructor(private postService: PostService) { }

  ngOnInit() {
    this.timeNow = Date.now();
    this.postService.getComments(this.postId).subscribe(res => this.comments = res);
  }

}
