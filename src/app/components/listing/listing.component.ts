import { Component, OnInit } from '@angular/core';
import { IPost } from 'src/app/shared/models/posts/post';
import { PostService } from 'src/app/services/post/post.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

  public listing: IPost[];
  constructor(private postService: PostService) { }

  ngOnInit() {
    this.postService.getPostsAll().subscribe(res => this.listing = res );
  }

}
