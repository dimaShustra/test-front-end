export const endpoint = {
    posts: {
        getAll: '/posts',
        get: '/posts/'
    },
    users: {
        get: '/users/'
    }
};
